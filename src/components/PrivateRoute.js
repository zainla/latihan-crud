import React, {useState, Component} from 'react'
import { Redirect, Route } from 'react-router-dom'
import AuthService from './../service/AuthService'
import Session from '../Session/Session';

const PrivateRoute = ({ component: Component, ...rest}) =>{
    const [logic, setLogic] = useState(true);
    const [loading, setLoading] = useState(true);

   // AuthService.checkLogin({token : "Bearer"+Session.getSession('token')})
function data ()
{
  // setLogic(logic = true);  
  setLogic(true);
  setLoading(true);
  console.log("data = ",data);
  console.log("berhasil = ",logic);

}
function error ()
{
  // console.log("gagal");
  setLogic(true);
  setLoading(true);
  console.log("gagal = ",logic);
  console.log(error);
};
console.log(logic);
  return (
    loading == true ? 
    <Route
      {...rest}
      render={props =>
        logic == true ? (
          
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        )
      }
    />
    : <div>Loading</div>
  )
}

export default PrivateRoute