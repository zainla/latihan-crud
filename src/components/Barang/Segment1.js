import React from 'react';
import {Layout, Table,Button, Input, Icon, Row, Col, Form, Popconfirm, message, Tag, Modal } from 'antd';
import {Resizable} from 'react-resizable';
import { Divider } from 'semantic-ui-react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";
// import Highlighter from 'react-syntax-highlighter-words';
//import BarangService from '../../service/BarangService';
import Segment1service from '../../service/Segment1service';
import CreateModal from './CreateModal';
import EditModal from'./EditModal';
import CreateSegment1 from './CreateSegment1';
import Updatesegment1 from './Updatesegment1';
import UpdateSegment1 from './Updatesegment1';
import CollectionCreateForm from './Updatesegment1';
import Session from './../../Session/Session';

const {Content} = Layout;
const key = 'updatable';
const ResizeableTitle = props =>{
    const { onResize,width,...restProps} = props;
    if (!width) {
        return <th {...restProps}/>;
    }
    
    return (
        <Resizable
        width={width}
        height={0}
        onResize={onResize}
        draggableOpts={{enableUserSeleckHack: false}}
        >

            <th {...restProps} />
        </Resizable>
    );
};

class Segment1 extends React.Component{
  // this.state = {
  //   visible: false,
  //   confirmLoading: false,
  //   show: false
  // };
  
  state = { 
    visible: false,
    confirmLoading: false,
    // show: false
   };
   
  showModal = () =>{
    this.setState({ visible: true});
  };

  hideModal = () => {
    this.setState({ visible: false });
  };
  
    constructor(props){
        super(props);

        this.state = {
            searchText: '',
            searchhedColumns: '',
            columns: [
                {
                    title: 'Id segment',
                    dataIndex: 'Segment1_ID',
                    key: 'Segment1_ID',
                    sorter: (a,b) => a.Segment1_ID.length - b.Segment1_ID.length,
                    sortDirection: ['descend', 'asccend'],
                },
                {
                    title: 'Nama segment',
                    dataIndex: 'Segment1_Name',
                    key: 'Segment1_Name',
                    sorter: (a,b) => a.Segment1_Name.length - b.Segment1_Name.length,
                    sortDirection: ['descend', 'asccend'],
                },
                {
                    title: 'Type segment',
                    dataIndex: 'Segment1_Type',
                    key: 'Segment1_Type',
                    sorter: (a,b) => a.Segment1_Type.length - b.Segment1_Type.length,
                    sortDirection: ['descend', 'asccend'],
                },
                {
                    title: 'active segment',
                    dataIndex: 'Segment1_ActiveYN',
                    key: 'Segment1_ActiveYN',
                    sorter: (a,b) => a.Segment1_ActiveYN.length - b.Segment1_ActiveYN.length,
                    sortDirection: ['descend', 'asccend'],
                },
                {
                    title: 'Action',
                    dataIndex: 'action',
                    key: 'action',
                    // render: (text, record, ) =>
                    //     this.state.data.length >=1 ? (
                          
                    //     <span>
                    //         <Button size="small" onClick={this.showModal} value={record.key}>
                    //             Edit Data
                    //         </Button> 
                    //     </span>
                    // ) : null,
                },
                
            ],
            data : []
        };
        
    }
    componentDidMount(){
        message.loading({ content: 'Loading....', key, duration: 0 });
        this.dataSegment();
    }
    dataSegment = () =>
    {
        Segment1service.getListSegment1({
          //parameter
           //company_id :1
           Segment1_ID : 0
        })
        .then((resp) =>
        {
            console.log(resp)
            let data = resp.data;
            let dataResp = [];
            for (var i =0; i < data.responseData.length; i++) {
                let obj = data.responseData[i]
                dataResp.push({
                    key:i,
                    Segment1_ID: obj.Segment1_ID,
                    Segment1_Name: obj.Segment1_Name,
                    Segment1_Type: obj.Segment1_Type,
                    Segment1_ActiveYN: obj.Segment1_ActiveYN,
                    status: (obj.Segment1_ActiveYN == "y" ? <Tag color="green" key="active">Active</Tag>
                    : <Tag color="volcano" key="inactive">In Active</Tag>),
                    action: <Button onClick={() => this.editData(obj.Segment1_ID,obj.Segment1_Name,obj.Segment1_Type,obj.Segment1_ActiveYN)}>Edit Data</Button>
                })
            }
            // console.log(resp);
            this.setState({data : dataResp});
            message.success({ content: 'Loaded!', key, duration: 2 });
        })
        .catch((err) =>
        {
            message.error({ content: err.message, key, duration: 2});
        });
    }

    getColumnSearchProps = (dataIndex, title) => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
            <div style={{ padding: 8 }}>
	        <Input
	          ref={node => {
	            this.searchInput = node;
	          }}
	          placeholder={`Search ${title}`}
	          value={selectedKeys[0]}
	          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
	          onPressEnter={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
	          style={{ width: 188, marginBottom: 8, display: 'block' }}
	        />
	        <Button
	          type="primary"
	          onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
	          icon="search"
	          size="small"
	          style={{ width: 90, marginRight: 8 }}
	        >
	          Search
	        </Button>
	        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
	          Reset
	        </Button>
	      </div>
        ),
        filterIcon: filtered => (
            <Icon type="search" style={{color: filtered ? '#1890ff' : undefined}}/>
        ),
        onFilter: (value,record) => 
        record[dataIndex]
        .toString()
        .tolowerCase()
        .includes(value.tolowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(()=> this.searchInput.select());
            }
        },
        render: text =>
            // this.state.searchedColumn === dataIndex ? (
            //     <Highlighter
            //     highlighStyle={{ backgroundColor: '#ffc069', padding: 0 }}
            //     searchWords={[this.state.searchText]}
            //     autoEscape
            //     textToHighlight={text.toString()}
            //     />
            // ) : (
                text
            // ), 
    });

    handleSearch = (selectedKeys, confirm, dataIndex) => {
	    confirm();
	    this.setState({
	      searchText: selectedKeys[0],
	      searchedColumn: dataIndex,
	    });
    };

    handleReset = clearFilters =>{
        clearFilters();
        this.setState({ searchText: ''});
    };

    handleDelete = dataEditId => {
        const dataSource = [...this.state.data];
        this.setState({data: dataSource.filter(item => item.id !== dataEditId) });
        message.success({content: 'Success delete data!', duration: 2 });
        this.setState({ visible: false });
    };

    components = {
        header: {
            cell: ResizeableTitle,
        },
    };
    

    handleOk = () => {
        // const { form } = this.formRef.props;
        // form.validateFields((err, values) => {
        //     if (err) {
        //         return;
        //     }

        // this.setState({
        //     confirmLoading: true,
        // });

        // console.log('values = ',values);
        // console.log("modaladdsegment1id = ", Session.getSession('userId '));
        Segment1service.updateSegment1({
            user_id: 0,
            //barangNama: values.barangNama,
            modaleditsegment1id: this.state.dataEditId,
            modaleditsegment1name: this.state.dataEditName,
            modaleditsegment1_select: this.state.dataEditActive,
            modaleditsegment1type: this.state.dataEditType,
                
        }).then((resp) => {
            let data = resp.data
            if (data.responseCode === "00") {
            // form.resetFields();
            message.success({ content: data.responseDesc, duration: 2 });
            // this.props.reInit()
            this.componentDidMount()
            }else{
            message.error({ content: data.responseDesc, duration: 2 });
            }
            this.setState({ 
            visible: false,
            confirmLoading: false,
            });
        }).catch((e) => {
            this.setState({ 
            visible: false,
            confirmLoading: false,
            });
            message.error({ content: e.message, duration: 2 });
        })
        // });
    };

    saveFormRef = formRef => {
        this.formRef = formRef;
      };

    handleCancel = () => {
      this.setState({ visible: false });
    };

    handleResize = index => (e, { size }) => {
		this.setState(({ columns }) => {
		  const nextColumns = [...columns];
		  nextColumns[index] = {
		    ...nextColumns[index],
		    width: size.width,
		  };
		  return { columns: nextColumns };
		});
    };

    editData(id, name, type, active)
    {
        this.setState({ 
            visible: true,
            dataEditId: id,
            dataEditName: name,
            dataEditType: type,
            dataEditActive: active,
        });
    }

    onTodoChange(val,value){
        if(val === '1')
        {
            this.setState({
                dataEditId: value
            });
        }
        else if (val === '2')
        {
            this.setState({
                dataEditName: value
            });
        }
        else if (val === '3')
        {
            this.setState({
                dataEditType: value
            });
        }
        else if (val === '4')
        {
            this.setState({
                dataEditActive: value
            });
        }
    }

    render() {
		const columns = this.state.columns.map((col, index) => {
            // console.log("log = ",col);
			if (col.dataIndex && col.dataIndex !== "Segment1_ID" && col.dataIndex !== "Segment1_Name") {
				return {
			      ...col,
			      ...this.getColumnSearchProps(col.dataIndex, col.title),
			      onHeaderCell: column => ({
			        width: column.width,
			        onResize: this.handleResize(index),
			      }),
			    }
			}else{
				return {
			      ...col,
			      onHeaderCell: column => ({
			        width: column.width,
			        onResize: this.handleResize(index),
			      }),
			    }
			}
        });
        
        return (
			<Content
	            style={{
	              margin: '24px 16px',
	              padding: 24,
	              background: '#fff',
	              minHeight: 280,
	            }}
	        >
            
	        	{<CreateSegment1 reInit={this.dataSegment}/>}
				<Table bordered components={this.components} columns={columns} dataSource={this.state.data} size="middle" />

                <Modal
                    title="Basic Modal"
                    // visible={this.state.confirmLoading}
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    >      
                        <Form layout="vertical">
                            <Row gutter={16}>
                            <Col className="gutter-row" span={12}>
                                <div className="gutter-box">
                                    <Form.Item label="ID Segment1">
                                        <Input onChange={e => this.onTodoChange('1',e.target.value)} value={this.state.dataEditId} required/>
                                    </Form.Item>
                                    <Form.Item label="Nama Segment1" >
                                        <Input onChange={e => this.onTodoChange('2',e.target.value)} value={this.state.dataEditName}/>
                                    </Form.Item>
                                    <Form.Item label="Type Segment1">
                                        <Input onChange={e => this.onTodoChange('3',e.target.value)} value={this.state.dataEditType}/>
                                    </Form.Item>
                                    <Form.Item label="Active Segment1">
                                        <Input onChange={e => this.onTodoChange('4',e.target.value)} value={this.state.dataEditActive}/>
                                    </Form.Item>
                                </div>
                            </Col>
                            </Row>
                        </Form>
                    {/* <CollectionCreateForm reInit={this.getList} dataRecord={record} />  */}
                    <Divider type="vertical" /> 
                    <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete()}>
                        <Button size="small" type="danger" icon="delete">Delete</Button>
                    </Popconfirm>
                    {/* <Popconfirm title="Sure to Update?" onConfirm={() => this.handleUpdate()}>
                        <Button size="small" type="" icon="update">Update</Button>
                    </Popconfirm> */}
                </Modal>
	        </Content>
		);
  }
     

}
export default Segment1;