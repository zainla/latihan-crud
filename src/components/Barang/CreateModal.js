import React from 'react';
import { Button, Modal, Form, Input, message, Row, Col, InputNumber, Select } from 'antd';

// import EmployeeService from './Service/EmployeeService';
// import PositionEmployeeService from './Service/PositionEmployeeService';
// import DivisionService from './Service/DivisionService';
import Session from './../../Session/Session';
import BarangService from '../../service/BarangService';

const { TextArea } = Input;
const { Option } = Select;

const CollectionCreateForm = Form.create({ name: 'form_in_modal' })(
  // eslint-disable-next-line
  class extends React.Component {

    state = {
      dataPosition: [],
      dataDivision: [],
    }
    
    componentDidMount() {
    //   this.getPosition()
    //   this.getDivision()
    }
    render() {
      const { visible, onCancel, onCreate, form, confirmLoading } = this.props;
      const { getFieldDecorator } = form;
      return (
        <Modal
          visible={visible}
          title="Create data"
          okText="Create"
          onCancel={onCancel}
          onOk={onCreate}
          confirmLoading={confirmLoading}
        >
          <Form layout="vertical">
            <Row gutter={16}>
              <Col className="gutter-row" span={12}>
                <div className="gutter-box">
                  <Form.Item label="Nama Barang">
                    {getFieldDecorator('barangNama', {
                      rules: [{ required: true, message: 'Nama barang' }],
                    })(<Input />)}
                  </Form.Item>
                  
                </div>
              </Col>
              
            </Row>
          </Form>
        </Modal>
      );
    }
  },
);

class CreateModal extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      visible: false,
      confirmLoading: false,
    };
  }

  showModal = () => {
    this.setState({ visible: true });
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  handleUpdate = () => {};

  handleCreate = () => {
    const { form } = this.formRef.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }

      this.setState({
        confirmLoading: true,
      });

      console.log('values = ',values.barangNama);
      console.log("companyId = ", Session.getSession('companyId'));
      BarangService.saveBarang({
          company_id: Session.getSession('companyId'),
          username: Session.getSession('username'),
          barangNama: values.barangNama,
      }).then((resp) => {
        let data = resp.data
        if (data.responseCode === "00") {
          form.resetFields();
          message.success({ content: data.responseDesc, duration: 2 });
          this.props.reInit()
        }else{
          message.error({ content: data.responseDesc, duration: 2 });
        }
        this.setState({ 
          visible: false,
          confirmLoading: false,
        });
      }).catch((e) => {
        this.setState({ 
          visible: false,
          confirmLoading: false,
        });
        message.error({ content: e.message, duration: 2 });
      })
    });
    
  };

  saveFormRef = formRef => {
    this.formRef = formRef;
  };

  render() {
    return (
      <div>
        <Button type="primary" onClick={this.showModal} style={{ marginBottom: 16, display: 'flex',
  justifyContent : 'left' }} icon="plus">
          Create New
        </Button>
        <CollectionCreateForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          onCreate={this.handleCreate}
          confirmLoading={this.state.confirmLoading}
        />
      </div>
    );
  }
}

export default CreateModal