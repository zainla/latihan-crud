import React from 'react';
import { Button, Modal, Form, Input, message, Row, Col, InputNumber, Select } from 'antd';

// import EmployeeService from './Service/EmployeeService';
// import PositionEmployeeService from './Service/PositionEmployeeService';
// import DivisionService from './Service/DivisionService';
import Session from './../../Session/Session';
import BarangService from '../../service/BarangService';
import Segment1service from '../../service/Segment1service';
import Segment1 from './Segment1';

const { TextArea } = Input;
const { Option } = Select;

const CollectionCreateForm = Form.create({ name: 'form_in_modal' })(
  // eslint-disable-next-line
  class extends React.Component {

    state = {
      dataPosition: [],
      dataDivision: [],
    }
    
    componentDidMount() {
    //   this.getPosition()
    //   this.getDivision()
    }
    render() {
      const { visible, onCancel, onCreate, form, confirmLoading } = this.props;
      const { getFieldDecorator } = form;
      return (
        <Modal
          visible={visible}
          title="Create data"
          okText="Create"
          onCancel={onCancel}
          onOk={onCreate}
          confirmLoading={confirmLoading}
        >
          <Form layout="vertical">
            <Row gutter={16}>
              <Col className="gutter-row" span={12}>
                <div className="gutter-box">
                  <Form.Item label="ID Segment1">
                    {getFieldDecorator('modaladdsegment1id', {
                      rules: [{ required: true, message: 'ID Segment1' }],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Nama Segment1">
                    {getFieldDecorator('modaladdsegment1name', {
                      rules: [{ required: true, message: 'Nama Segment1' }],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Type Segment1">
                    {getFieldDecorator('modaladdsegment1type', {
                      rules: [{ required: true, message: 'Type Segment1' }],
                    })(<Input />)}
                  </Form.Item>
                  <Form.Item label="Active Segment1">
                    {getFieldDecorator('modaladdsegment1_select', {
                      rules: [{ required: true, message: 'Active Segmnent1' }],
                    })(<Input />)}
                  </Form.Item>
                  
                </div>
              </Col>
              
            </Row>
          </Form>
        </Modal>
      );
    }
  },
);

class CreateSegment1 extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      visible: false,
      confirmLoading: false,
    };
  }

  showModal = () => {
    this.setState({ visible: true });
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  handleUpdate = () => {};

  handleCreate = () => {
    const { form } = this.formRef.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }

      this.setState({
        confirmLoading: true,
      });

      console.log('values = ',values);
      console.log("modaladdsegment1id = ", Session.getSession('userId '));
      Segment1service.saveSegment1({
          user_id: 1,
          //barangNama: values.barangNama,
            modaladdsegment1id: values.modaladdsegment1id,
            modaladdsegment1name: values.modaladdsegment1name,
            modaladdsegment1_select: values.modaladdsegment1_select,
            modaladdsegment1type: values.modaladdsegment1type,
            
      }).then((resp) => {
        let data = resp.data
        if (data.responseCode === "00") {
          form.resetFields();
          message.success({ content: data.responseDesc, duration: 2 });
          this.props.reInit()
          
        }else{
          message.error({ content: data.responseDesc, duration: 2 });
        }
        this.setState({ 
          visible: false,
          confirmLoading: false,
        });
      }).catch((e) => {
        this.setState({ 
          visible: false,
          confirmLoading: false,
        });
        message.error({ content: e.message, duration: 2 });
      })
    });
  };

  saveFormRef = formRef => {
    this.formRef = formRef;
  };

  render() {
    return (
      <div>
        <Button type="primary" onClick={this.showModal} style={{ marginBottom: 16, display: 'flex',
  justifyContent : 'left' }} icon="plus">
          Create New
        </Button>
        <CollectionCreateForm
          wrappedComponentRef={this.saveFormRef}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          onCreate={this.handleCreate}
          confirmLoading={this.state.confirmLoading}
        />
      </div>
    );
  }
}

export default CreateSegment1