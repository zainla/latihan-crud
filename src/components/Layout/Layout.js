import React, { Component } from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useHistory,
  useLocation
} from "react-router-dom";
import { Layout, Menu, Icon } from 'antd';
import EditModal from'./../Barang/EditModal';
import Button from './../Barang/ListBarang'; 
import Barang from './../Barang/Barang';
import Segment1 from './../Barang/Segment1';
import PrivateRoute from './../PrivateRoute';
import { Segment } from 'semantic-ui-react';
import UpdateSegment1 from '../Barang/Updatesegment1';
const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

class Layout2 extends Component {
  
    handleClick = (item,key, selectedKeys) =>
    {
      // console.log(item,"item");
      // console.log(key,"key");
      window.location.href = item.key;
      // window.location.href = e;
    }
    render() {
      // const { location: { pathname } } = this.props;
        return (
          // <Router>
            <Layout>
              <Sider
                style={{
                  overflow: 'auto',
                  height: '100vh',
                  position: 'fixed',
                  left: 0,
                }}
              >
                <div className="logo" />
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
                  <Menu.Item key="1">
                    <Icon type="user" />
                    <span className="nav-text"><Link to="/penjualan" style={{ textDecoration: 'none' }}>Penjualan</Link></span>
                  </Menu.Item>
                  <Menu.Item>
                    
                    <Icon type="video-camera" />
                    <span className = "nav-text">about</span>
                    
                  </Menu.Item>
                  
                  <Menu.Item>
                    
                    <Icon type="video-camera" />
                    <span className = "nav-text"><Link to="/barang" style={{ textDecoration: 'none' }}>Barang</Link></span>
                    
                  </Menu.Item>
                  <SubMenu
                        key="masterData"
                        title={
                          <span>
                            <Icon type="appstore" />
                            <span>Master Data</span>
                          </span>
                        }
                      >
                        
                        <Menu.Item key="barang"><Link to="/barang" style={{ textDecoration: 'none' }}>Barang</Link></Menu.Item>
                        {/* <Menu.item key="listbarang">List Barang</Menu.item> */}
                        <Menu.Item key="jabatan">Jabatan</Menu.Item>
                        <Menu.Item key="segment1"><Link to="/segment1"  style={{ textDecoration: 'none' }}>Segment1</Link></Menu.Item>
                        <Menu.Item key="6">Alex</Menu.Item>
                    </SubMenu>
                  <Menu.Item key="5">
                    <Icon type="cloud-o" />
                    <span className="nav-text">nav 5</span>
                  </Menu.Item>
                  <Menu.Item key="8">
                    <Icon type="appstore-o" />
                    <span className="nav-text">nav 6</span>
                  </Menu.Item>
                  <Menu.Item key="9">
                    <Icon type="team" />
                    <span className="nav-text">nav 7</span>
                  </Menu.Item>
                  <Menu.Item key="10">
                    <Icon type="shop" />
                    <span className="nav-text">nav 8</span>
                  </Menu.Item>
                </Menu>
              </Sider>
              <Layout style={{ marginLeft: 200 }}>
                <Header style={{ background: '#fff', padding: 0 }} />
                <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
                  <div style={{ padding: 24, background: '#fff', textAlign: 'center',
                        minHeight: 280,
                  }}>
                      <PrivateRoute path="/barang" component={Barang}/>
                      <PrivateRoute path="/listBarang" component={Button}/>
                      <PrivateRoute path="/editmodal" component={EditModal}/>
                      <PrivateRoute path="/segment1" component={Segment1}/>
                      <PrivateRoute path="/updatesegment1" component={UpdateSegment1}/>
                      
                      
                  </div>
                </Content>
                <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
              </Layout>
            </Layout>
        );
    }
}
export default Layout2;