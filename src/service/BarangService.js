import API from './../Config/Api';
import axios from 'axios';

export default {
    getListBarang(request){
        return API.post(`barangList/a`, request)
    },

    saveBarang(request){
        return API.post("saveBarang",request)
    },

    updateBarang(request){
        return API.post("updateBarang",request)
    },
}
