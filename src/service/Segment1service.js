import API from './../Config/Api';
import APIGet from './../Config/ApiGet';
import axios from 'axios';

export default {
    getListSegment1(request){
        return API.get("segment1", request)
    },

    saveSegment1(request){
        return API.post("save-segment1",request)
    },

    updateSegment1(request){
        return API.post("update-segment1",request)
    },
    getDetailSegment1(request){
        return API.post("getDetailSegment1",request)
    },
}
