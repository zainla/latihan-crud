import axios from 'axios';
import Session from './../Session/Session';

export default axios.create({
    // baseURL: `http://54.179.184.177:8090/taskmanagement-0.0.1-SNAPSHOT/api/`,
  baseURL: `http://18.189.31.121:8080/fico-0.0.1-SNAPSHOT/api`,
  // baseURL: 'URL	http://18.189.31.121:8080/akuntansi-0.0.1-SNAPSHOT/api/barangList/a',
  // baseURL: `http://localhost:8090/api/`,  
   headers: {
      'Authorization': `Bearer ${Session.getSession('token')}`,
     'Content-Type': 'application/json',
    }
  });